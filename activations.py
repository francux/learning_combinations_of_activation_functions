import keras.activations
import keras.backend as K
import numpy as np
from keras.constraints import Constraint
from keras.engine.topology import InputSpec, Layer
from keras.initializers import Initializer


class UnitNormP(Constraint):
    def __init__(self, p=1, axis=0):
        self.axis = axis
        self.p = p

    def __call__(self, w):
        return w / (K.epsilon() + K.pow(K.sum(K.pow(K.abs(w), self.p),
            axis=self.axis,
            keepdims=True), 1. / self.p))

    def get_config(self):
        return {'axis': self.axis, 'p': self.p}


class UnitPInitializer(Initializer):
    def __init__(self, p=1):
        self.p = p

    def __call__(self, shape, dtype=None):
        unnormalized_probs = np.random.rand(*shape)
        probs = unnormalized_probs / np.power(sum(np.power(unnormalized_probs, self.p)), 1. / self.p)
        return K.constant(probs, shape=shape, dtype=dtype)

    def get_config(self):
        return {'p': self.p}


class CentroidInitializer(Initializer):

    def __init__(self, p=1):
        self.p = p

    def __call__(self, shape, dtype=None):
        num_of_params = shape[0]
        probs = [1. / (num_of_params ** (1. / self.p))] * num_of_params
        return K.constant(probs, shape=shape, dtype=dtype)

    def get_config(self):
        return {'p': self.p}


class ConvexActivation(Layer):
    def __init__(self, activations, alpha_initializer=None, alpha_constraint=None, **kwargs):
        super(ConvexActivation, self).__init__(**kwargs)
        self.activations = list(
            map(
                keras.activations.get,
                activations
            )
        )
        self.alpha_initializer = alpha_initializer or UnitPInitializer()
        self.alpha_constraint = alpha_constraint or UnitNormP()
        self.supports_masking = True

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = input_shape[-1]

        self.alpha = self.add_weight(
            shape=(len(self.activations),),
            initializer=self.alpha_initializer,
            name='convex_weights',
            regularizer=None,
            constraint=self.alpha_constraint,
        )

        self.input_spec = InputSpec(min_ndim=2, axes={-1: input_dim})
        self.built = True

    def call(self, inputs):
        activated = list(map(lambda a: K.expand_dims(a(inputs)), self.activations))
        return K.sum(K.concatenate(activated) * K.abs(self.alpha), axis=-1)


class AffineActivation(Layer):
    def __init__(self, activations, **kwargs):
        super(AffineActivation, self).__init__(**kwargs)
        self.activations = list(
            map(
                keras.activations.get,
                activations
            )
        )
        self.supports_masking = True

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = input_shape[-1]

        self.alpha = self.add_weight(
            shape=(len(self.activations) - 1,),
            initializer=CentroidInitializer(),
            name='affine_weights',
            regularizer=None,
            constraint=None,
        )

        self.input_spec = InputSpec(min_ndim=2, axes={-1: input_dim})
        self.built = True

    def call(self, inputs):
        activated = list(map(lambda a: K.expand_dims(a(inputs)), self.activations))
        actual_alpha = K.concatenate([self.alpha, K.expand_dims(1. - K.sum(self.alpha))])
        return K.sum(K.concatenate(activated) * actual_alpha, axis=-1)
