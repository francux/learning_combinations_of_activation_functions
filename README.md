# Learning Combinations of Activation Functions

Repository containing the Keras Activation layers implementing the learnable activation functions introduced in [1801.09403](https://arxiv.org/abs/1801.09403)


## Usage
The file `activations.py` contains the two classes `ConvexActivation` and `AffineActivation`. Both of them are supposed to be used as regular `keras.Layer` classes. Their `__init__` method requires you to specify the collection of base activation functions to be used.

### Example
```python
from keras.models import Model
from keras.layers import Activation, Dense, Input

from activations import AffineActivation, ConvexActivation

# MLP with ConvexActivation using 'linear', 'relu', 'tanh' as base activation functions
input_ = Input(shape=(32,))
x = Dense(32)(x)
x = ConvexActivation(('linear', 'relu', 'tanh'))(x)
x = Dense(10)(x)
output = Activation('softmax')(x)
convex_activation_model = Model(inputs=input_, outputs=output)

# MLP with AffineActivation using 'linear', 'relu' as base activation functions
input_ = Input(shape=(32,))
x = Dense(32)(x)
x = AffineActivation(('linear', 'relu'))(x)
x = Dense(10)(x)
output = Activation('softmax')(x)
affine_activation_model = Model(inputs=input_, outputs=output)
```
